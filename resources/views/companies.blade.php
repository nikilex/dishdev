@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-between">
        @foreach($companies as $company)
        <div class="col-3 company">
            <a href="{{ route('company', ['id' => $company->id]) }}">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="img">
                            <img src="{{ asset('img/bk.webp') }}" alt="">
                        </div>
                    </div>
                    <div class="col-12 py-2">
                        <h4>{{ $company->name }}</h4>
                        <p>г. {{ $company->city->name }}</p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection