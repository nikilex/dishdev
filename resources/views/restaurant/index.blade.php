@extends('layouts.app')

@section('content')
<div class="rst-top p-4">
    <div class="container h-100">
        <div class="row mx-0 h-100 align-items-center">
            <div class="col-3 text-center">
                <img src="{{ asset('img/appetitus.jpeg') }}" class="rst-logo" alt="">
            </div>
            <div class="col-sm-6 p-md-0 row align-items-center">

                    <div class="col-12">
                        <div class="welcome-text">
                            <h3>{{ $restaurant->name }}</h3>
                            <p class="mb-0">Your business dashboard template</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <hr class="rst-top-hr">
                    </div>
                    <div class="col">
                        <span class="rst-desc-item">Мин. заказ:</span> <br>
                        300р
                    </div>
                    <div class="col">
                    <span class="rst-desc-item">Доставка:</span> <br>
                        60р
                    </div>
                    <div class="col">
                    <span class="rst-desc-item">Время доставки:</span> <br>
                        До 1,5 часов
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-4">
        <div class="col">
            @foreach($restaurant->categories as $category)
            @if(!$category->dishes->isEmpty())
            <span class="mr-3"><a href="#category-{{ $category->id }}">{{ $category->name }}</a></span>
            @endif
            @endforeach
        </div>
    </div>
    <div class="row">
        @foreach($restaurant->categories as $category)
        @if(!$category->dishes->isEmpty())
        <div class="col-12 mt-3" id="category-{{ $category->id }}">
            <h3>{{ $category->name }}</h3>
        </div>


        @foreach($category->dishes as $dish)
        <div class="col-xl-4 col-lg-6 col-md-6 col-12 mt-3">
            <div class="card dish-card">
                <div class="card-body p-0">
                    <div class="new-arrival-product">
                        <div class="new-arrivals-img-contnent dish-image-box">
                            <img class="img-fluid" src="{{ Storage::disk('public')->has($dish->image) ?  Storage::disk('public')->url($dish->image) : asset('img/basket.jpg') }}" alt="">
                        </div>
                        <div class="row dish-bottom p-3">
                            <div class="col-12 new-arrival-content mt-3">
                                <h4>{{ $dish->name }}</h4>
                                <p>{{ $dish->weight }} {{ $dish->unit->name}}</p>
                                <input type="hidden" name="dishId" value="{{ $dish->id }}">
                                <input type="hidden" name="rstId" value="{{ $restaurant->id }}">
                            </div>
                            <!--Quantity start-->
                            <div class="col mt-3">
                                <div class="row justify-content-between">
                                    <!-- <div class="col-5">
                                    <input style="height: 42px" type="number" name="num" class="form-control input-btn input-number" value="1" >
                                </div> -->
                                    <div class="col price-box">
                                        <span class="price">{{ $dish->price }}р.</span>
                                    </div>
                                    <div class="col text-right shopping-cart">
                                        <button class="btn btn-primary btn-sm to-cart">
                                            <div class="btn-text">
                                                <i class="fa fa-shopping-basket mr-2"></i>В корзину
                                            </div>
                                            <div class="spinner-border text-light d-none card-spinner" role="status">
                                                <!-- <span class="visually-hidden">Загрузка...</span> -->
                                            </div>
                                        </button>
                                    </div>
                                    <div class="col text-right shopping-cart-count justify-content-end d-none">
                                        <button type="button" class="btn btn-outline-light btn-xxs btn-minus">-</button>
                                        <input type="text" class="form-control input-default count-dish-in-cart" placeholder="" value="0">
                                        <button type="button" class="btn btn-outline-light btn-xxs btn-plus to-cart">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        @endif
        @endforeach
    </div>
</div>

@endsection

@section('js')
<script>
    updateCart();
    eachCardInCart();
    /**
     * {
     * 
     *  rst_id: 15, 
     *  dishes: [
     *      { 
     *          id:    12
     *          name:  'Ролы Филадельфия'
     *          price: 124
     *          count: 1  
     *      },
     *      { 
     *          id:    17
     *          name:  'Картошка Фри'
     *          price: 250
     *          count: 3
     *      },
     *  ]
     * 
     * }
     */
    function getCart() {
        let cart = localStorage.getItem('cart');
        let defaultCart = {
            rst_id: null,
            dishes: []
        }
        if (cart != null) {
            return JSON.parse(cart);
        } else {
            localStorage.setItem('cart', JSON.stringify(defaultCart));
            return defaultCart;
        }
    }

    function eachCardInCart() {
        let cards = $('.card');

        cards.each(function(index, value) {
            let dishId = $(this).find('[name="dishId"]');
            let card = $(this);
            let buttonBox = card.find('.shopping-cart');
            let countButtons = card.find('.shopping-cart-count');
            let btnText = card.find('.btn-text');
            let countInput = card.find('.count-dish-in-cart');
            let dishItemInCart = getItemInCart(dishId.val());

            if (dishItemInCart) {
                buttonBox.addClass('d-none');
                countButtons.removeClass('d-none').addClass('d-flex');
                countInput.val(dishItemInCart.count);
            }
        });
    }

    function inCart(dishId) {
        let cart = getCart();

        for (let dish of cart.dishes) {
            if (dish.id == dishId) {
                return true
            }
        }

        return false;
    }

    function getItemInCart(dishId) {
        let cart = getCart();

        for (let dish of cart.dishes) {
            if (dish.id == dishId) {
                return dish;
            }
        }

        return false;
    }

    function findDishIdInCart(dishesArr, searchId) {
        let cartDishId = dishesArr.findIndex(dish => dish.id === searchId)
        return cartDishId;
    }

    function updateCart() {
        let cart = getCart();
        let cartHTML = $('#cart').find('.cart__item-box');
        let spanCount = $('.count-cart');
        let html = '';
        let allCount = 0;

        for (let dish of cart.dishes) {
            html += '<div class="cart__item">\
                        <div class="cart__item-name">' + dish.name + '</div>\
                        <div class="cart__item-count">' + dish.count + '</div>\
                        <div class="cart__item-price">' + dish.price * dish.count + '</div>\
                    </div>';
            allCount += dish.count;
        }

        cartHTML.html(html);
        spanCount.html(allCount);
    }


    $('.to-cart').on('click', function() {
        let cart = getCart();
        let dishId = $(this).closest('.card').find('[name="dishId"]');
        let rst = $(this).closest('.card').find('[name="rstId"]');
        let card = $(this).closest('.card');
        let buttonBox = card.find('.shopping-cart');
        let countButtons = card.find('.shopping-cart-count');
        let spinner = buttonBox.find('.card-spinner');
        let btnText = card.find('.btn-text');
        let countInput = card.find('.count-dish-in-cart');

        btnText.addClass('d-none');
        spinner.removeClass('d-none');

        $.post('/admin/dish/getDishById', {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            dishId: dishId.val()
        }, function(res) {
            spinner.addClass('d-none');
            buttonBox.addClass('d-none');
            countButtons.removeClass('d-none').addClass('d-flex');

            cart.rst_id = rst.val();

            // проверяем есть ли в корзине такой товар с таким идентификатором
            let cartDishId = findDishIdInCart(cart.dishes, res.id);

            // если товар найден в корзине, то прибавляем количество
            // иначе добавляем новый товар
            if (cartDishId == -1) {
                cart.dishes.push({
                    id: res.id,
                    name: res.name,
                    price: res.price,
                    count: 1
                })
                let cartNewDishId = findDishIdInCart(cart.dishes, res.id);
                countInput.val(cart.dishes[cartNewDishId].count);
            } else {
                cart.dishes[cartDishId].count++;
                countInput.val(cart.dishes[cartDishId].count);
            }

            

            localStorage.setItem('cart', JSON.stringify(cart));
            updateCart();
        });
    })

    $('#send-order').on('click', function() {
        let cart = getCart();

        $.post('/admin/orders/store', {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            cart: cart
        }, function(res) {
            console.log(res);
        })
    })
</script>
@endsection