@extends('layouts.app')

@section('content')
<div class="container">
    <h1>{{ $company->name }}</h1>
    <div class="row set-of-dish justify-content-between">
        @foreach($company->dishes as $dish)
        <div class="border dish d-flex flex-column justify-content-between">
            <div class="img">
                <img src="{{ asset('img/basket.jpg') }}" alt="">
            </div>
            <div class="content p-3">
                <div class="head">
                    <div class="row">
                        <div class="col-8">
                            <h5>{{ $dish->name }}</h5>
                            <p>{{ $dish->weight }} {{ $dish->unit->name}}</p>
                        </div>
                        <div class="col-4 price text-center">
                            {{ $dish->price }}р.
                        </div>
                    </div>

                </div>
                <div class="row">
                        <div class="col">
                            {{ $dish->description }}
                        </div>
                    </div>
                <div class="row">
                    <div class="col d-flex justify-content-end">
                        <div class="info mr-3">
                            <i class="fas fa-info-circle"></i>
                        </div>
                        <div class="cart mr-3">
                            <i class="fas fa-cart-arrow-down"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection