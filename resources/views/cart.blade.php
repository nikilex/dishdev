@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row page-titles m-0 mx-0 mt-md-4">
        <div class="col-sm-6 p-0 p-md-2">
            <div class="welcome-text">
                <h4>Корзина</h4>
                <p class="mb-0">Your business dashboard template</p>
            </div>
        </div>
    </div>
    <div class="row cart card p-2 p-sm-3 p-md-5">
        <div class="col" id="cart">
            <div class="cart__item-box">
                <!-- <div class="cart__item">
                    <div class="cart__item-name"></div>
                    <div class="cart__item-count"></div>
                    <div class="cart__item-price"></div>
                </div> -->
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Название</th>
                            <th scope="col">Количество</th>
                            <th scope="col">Цена</th>
                        </tr>
                    </thead>
                    <tbody class="cart-table-body">
                    </tbody>
                    <tbody>
                        <tr>
                            <td>Итого: <span class="cart-summ">0</span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 text-right">
            <button class="btn btn-primary send-order" data-toggle="modal" data-target="#order">Сделать заказ</button>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="order">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Оформление заказа</h5>
            </div>
            <div class="modal-body">
                    <div class="mb-3">
                        <label for="phone" class="form-label">Телефон</label>
                        <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Адрес</label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="comment">Комментарий</label>
                        <input type="text" class="form-control" id="comment" name="comment">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="send-order">Заказать</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="thanks">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Заказ оформлен</h5>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    Спасибо за заказ!
                </div>
            </div>
            <div class="modal-footer">
                <a href="/" class="btn btn-secondary">На главную</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    updateCart();

    /**
     * {
     * 
     *  rst_id: 15, 
     *  dishes: [
     *      { 
     *          id:    12
     *          name:  'Ролы Филадельфия'
     *          price: 124
     *          count: 1  
     *      },
     *      { 
     *          id:    17
     *          name:  'Картошка Фри'
     *          price: 250
     *          count: 3
     *      },
     *  ]
     * 
     * }
     */
    function getCart() {
        let cart = localStorage.getItem('cart');
        let defaultCart = {
            rst_id: null,
            dishes: []
        }
        if (cart != null) {
            return JSON.parse(cart);
        } else {
            localStorage.setItem('cart', JSON.stringify(defaultCart));
            return defaultCart;
        }
    }

    function clearCart() {
        localStorage.removeItem('cart');
    }

    function findDishIdInCart(dishesArr, searchId) {
        let cartDishId = dishesArr.findIndex(dish => dish.id === searchId)
        return cartDishId;
    }

    function updateCart() {
        let cart = getCart();
        let cartHTML = $('#cart').find('.cart-table-body');
        let spanCount = $('.count-cart');
        let cartSummHtml = $('.cart-summ');
        let html = '';
        let allCount = 0;
        let summ = 0;

        for (const [index, dish] of cart.dishes.entries()) {
            html += '<tr>\
                        <td>' + (index + 1) + '</td>\
                        <td>' + dish.name + '</td>\
                        <td>' + dish.count + '</td>\
                        <td>' + dish.price * dish.count + '</td>\
                    </tr>';
            let proPrice = dish.price * dish.count;
            allCount += dish.count;
            summ += proPrice;
        }

        cartHTML.html(html);
        spanCount.html(allCount);
        cartSummHtml.html(summ);
    }


    $('.to-cart').on('click', function() {
        let cart = getCart();
        let dishId = $(this).closest('.card').find('[name="dishId"]');
        let rst = $(this).closest('.card').find('[name="rstId"]');

        $.post('/admin/dish/getDishById', {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            dishId: dishId.val()
        }, function(res) {
            cart.rst_id = rst.val();

            // проверяем есть ли в корзине такой товар с таким идентификатором
            let cartDishId = findDishIdInCart(cart.dishes, res.id);

            // если товар найден в корзине, то прибавляем количество
            // иначе добавляем новый товар
            if (cartDishId == -1) {
                cart.dishes.push({
                    id: res.id,
                    name: res.name,
                    price: res.price,
                    count: 1
                })
            } else {
                cart.dishes[cartDishId].count++;
            }

            localStorage.setItem('cart', JSON.stringify(cart));
            updateCart();
        });
    })

    $('#send-order').on('click', function() {
        let cart = getCart();
        let address = $('[name="address"]').val();
        let phone = $('[name="phone"]').val();
        let comment = $('[name="comment"]').val();
        let modalOrder = $('#order');
        let modalThanks = $('#thanks');
        let spanCount = $('.count-cart');
        let cartHTML = $('#cart');

        $.post('/admin/orders/store', {
            '_token': $('meta[name="csrf-token"]').attr('content'),
            cart: cart,
            address: address,
            phone: phone,
            comment: comment,
        }, function(res) {
            modalOrder.modal('hide');
            modalThanks.modal('show');
            clearCart();
            spanCount.html('0');
            cartHTML.html(' <table class="table">\
                    <thead>\
                        <tr>\
                            <th scope="col">#</th>\
                            <th scope="col">Название</th>\
                            <th scope="col">Количество</th>\
                            <th scope="col">Цена</th>\
                        </tr>\
                    </thead>\
                    <tbody class="cart-table-body">\
                    </tbody>\
                    <tbody>\
                        <tr>\
                            <td>Итого: <span class="cart-summ">0</span></td>\
                        </tr>\
                    </tbody>\
                </table>');
            console.log(res);
        })
    })
</script>
@endsection