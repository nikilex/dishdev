@extends('layouts.app')

@section('css')
@livewireStyles

<link rel="stylesheet" href="{{ asset('libs/owlcarousel/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('libs/owlcarousel/css/owl.theme.default.min.css') }}">
<style>
    .sale-img-container {
        margin-right: 20px;
        box-shadow: 0 7px 20px -8px rgb(157 157 163 / 50%);
        border-radius: 16px;
        width: 262px;
        overflow: hidden;
        height: 162px;
        transition: all 0.2s ease;
    }

    .sale-img-container:hover {
        box-shadow: 0 7px 19px -1px rgb(172 172 179 / 50%);
        transform: translateY(-6px);
    }
</style>
@endsection

@section('content')

<div class="container mt-5">
    <div class="row owl-carousel">
        @for ($i = 1; $i <= 5; $i++) 
        <div class="col sale-img-container p-0">
            <div class="">
                <img src="{{ asset('img/sale/' . $i . '.jpeg') }}" alt="">
            </div>
        </div>
        @endfor
</div>
<div class="row">
    <div class="col">
        <h1>Рестораны</h1>
    </div>
</div>
<div class="row">
    @foreach($companies as $company)
    <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6">
        <div class="card restaurant">
            <div class="card-body p-0">
                <a href="{{ route('rst.index', ['id' => $company->id]) }}">
                    <div class="new-arrival-product">
                        <div class="rst-con">

                        </div>
                        <div class="new-arrival-content text-center mt-3">
                            <h4>{{ $company->name }}</h4>
                            <p>г. {{ $company->city->name }}</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
</div>
@endsection

@section('js')
@livewireScripts

<script src="{{ asset('libs/owlcarousel/js/owl.carousel.min.js') }}"></script>

<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
</script>
@endsection