@extends('admin.layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/cropper.min.css') }}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <!-- <div class="welcome-text">
                <h4>Добавление блюда!</h4>
                <span>Element</span>
            </div> -->
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Редактирование блюда</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Данные блюда</h4>
                </div>
                <div class="card-body">
                    <div class="basic-form">
                        <form method="post" action="{{ route('admin.dish.update') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="file" class="form-control d-none" name="full-image" id="addImages">
                            <input type="hidden" class="form-control d-none" id="dataImg" name="id" value="{{ $dish->id }}">
                            <input type="hidden" name="top">
                            <input type="hidden" name="left">
                            <input type="hidden" name="width">
                            <input type="hidden" name="height">
                            <div class="form-group col">
                                <label>Название блюда</label>
                                <input type="text" class="form-control" name="name" value="{{ $dish->name }}">
                            </div>
                            <div class="form-group col">
                                <label>Категория</label>
                                <select class="form-control" name="category" id="category">
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if($dish->category_id == $category->id) selected @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label>Вес</label>
                                <input type="text" class="form-control" name="weight" value="{{ $dish->weight }}">
                            </div>
                            <div class="form-group col">
                                <label>Единица измерения</label>
                                <select class="form-control" name="unit" id="unit">
                                    @foreach($units as $unit)
                                    <option value="{{ $unit->id }}" @if($dish->unit_id == $unit->id)  selected @endif>{{ $unit->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label>Цена</label>
                                <input type="text" class="form-control" name="price" value="{{ $dish->price }}">
                            </div>
                            <div class="form-group col">
                                <label>Состав блюда</label>
                                <textarea class="form-control" name="description" rows="4" id="comment" >{{ $dish->description }}</textarea>
                            </div>
                            <div class="form-group col">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Изображение</h4>
                </div>
                <div class="card-body">
                    <div class="newImg" class="w-100">
                        <img src="{{ Storage::disk('public')->has($dish->image) ? url('storage/'. $dish->image) : ''}}" alt="" id="newImg">
                    </div>
                    <div class="addImage d-none">
                    </div>
                    <div class="button-block mt-2">
                        <button class="btn btn-primary" id="editImg">Изменить</button>
                        <button class="btn btn-danger"  id="deleteImg">Удалить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Обрезка изображения</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="image" src="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-primary" id="crop">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/cropper.min.js')}}"></script>
<!-- <script>
    $('.addImage, #editImg').on('click', function() {
        $('#addImages').trigger('click');
    });

    $('#deleteImg').on('click', function() {
        $('#addImages').val('');
        $('.newImg').find('#newImg').remove();
        $('.addImage').removeClass('d-none');
        $('#dataImg').val('');
        $('.button-block').addClass('d-none');
    });

    window.addEventListener('DOMContentLoaded', function() {
        var image = document.getElementById('image');
        var input = document.getElementById('addImages');
        
        var $modal = $('#modal');
        var cropper;

        $('[data-toggle="tooltip"]').tooltip();

        input.addEventListener('change', function(e) {
            var files = e.target.files;
            var done = function(url) {
                input.value = '';
                image.src = url;
                
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 16 / 9,
                zoomable: false,
            });
        }).on('hidden.bs.modal', function() {
            cropper.destroy();
            cropper = null;
        });

        document.getElementById('crop').addEventListener('click', function() {
            var canvas;

            $modal.modal('hide');

            if (cropper) {
                canvas = cropper.getCroppedCanvas();
                
                // Устанавливаем изображение
                $('.newImg').html($('<img>', {
                    id: 'newImg',
                    src: canvas.toDataURL(),
                }));

                $('.button-block').removeClass('d-none');
                $('.addImage').addClass('d-none');
                
               //$('#dataImg').val(canvas.toDataURL().split(';base64,')[1]);
               $('#dataImg').val(canvas.toDataURL());

                canvas.toBlob(function(blob) {
                    
                    var formData = new FormData();

                    formData.append('avatar', blob, 'avatar.jpg');
                });
            }
        });
    });
</script> -->

<script>
        $('.addImage, #editImg').on('click', function() {
            $('#addImages').trigger('click');
        });

        $('#deleteImg').on('click', function() {
            $('#addImages').val('');
            $('#newImg').attr('src', '');
            $('.addImage').removeClass('d-none');
            $('#dataImg').val('');
            $('.button-block').addClass('d-none');
        });

        window.addEventListener('DOMContentLoaded', function() {
            // var avatar = document.getElementById('avatar');
            var avatar = document.getElementById('newImg');

            var image = document.getElementById('image');
            var input = document.getElementById('addImages');
            // var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var formData = new FormData();
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function(e) {
                var files = e.target.files;
                var done = function(url) {
                    //  input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function(e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function() {
                cropper = new Cropper(image, {
                    aspectRatio: 16 / 9,
                    zoomable: false,
                    crop(event) {
                        let top = $('[name="top"]');
                        let left = $('[name="left"]');
                        let width = $('[name="width"]');
                        let height = $('[name="height"]');

                        top.val(event.detail.y);
                        left.val(event.detail.x);
                        width.val(event.detail.width);
                        height.val(event.detail.height);
                    },
                });
            }).on('hidden.bs.modal', function() {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function() {
                var initialAvatarURL;
                var canvas;
                var avatarImg;
                let boxAddImg = $('.addImage');
                let buttonBox = $('.button-block');


                $modal.modal('hide');
                boxAddImg.addClass('d-none');
                buttonBox.removeClass('d-none');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas(
                        //     {
                        //     width: 160,
                        //     height: 160,
                        // }
                    );
                    initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    $progress.show();
                    $alert.removeClass('alert-success alert-warning');
                    canvas.toBlob(function(blob) {
                        avatarImg = blob;
                        formData.append('avatar', blob, 'avatar.jpg');
                        formData.append('_token', $('meta[name="_token"]').attr('content'));
                        // console.log(blob);
                        // document.querySelector('#zdd').files = blob;
                        
                        // $.post("{{ route('admin.dish.store') }}", {
                        //     '_token': $('meta[name="_token"]').attr('content'),
                        //     'avatar': blob
                        // }, function(res) {
                        //     console.log(res);
                        // })
                    });
                }


            });



            // $('#addDish').on('click', function (e) {
            //     e.preventDefault();

            //     $.ajax('/admin/dish/store', {
            //                 method: 'POST',
            //                 data: formData,
            //                 processData: false,
            //                 contentType: false,

            //                 success: function() {
            //                     $alert.show().addClass('alert-success').text('Upload success');
            //                 },

            //                 error: function() {
            //                     avatar.src = initialAvatarURL;
            //                     $alert.show().addClass('alert-warning').text('Upload error');
            //                 },

            //                 complete: function() {
            //                     $progress.hide();
            //                 },
            //             });
            // })
        });
    </script>
@endsection