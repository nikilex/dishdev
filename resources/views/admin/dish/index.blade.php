@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <div class="welcome-text">
                <h4>Меню</h4>
            </div>
        </div>
        <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Админпанель</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Меню</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="ml-auto">
                        <a href="{{ route('admin.dish.add') }}">Добавить блюдо</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-responsive-md">
                            <thead>
                                <tr>
                                    <!-- <th style="width:50px;">
                                        <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                            <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                            <label class="custom-control-label" for="checkAll"></label>
                                        </div>
                                    </th> -->
                                    <th><strong>№</strong></th>
                                    <th><strong>Наименование</strong></th>
                                    <th><strong>Вес</strong></th>
                                    <th><strong>Цена</strong></th>
                                    <th><strong>Status</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dishes as $dish)
                                <tr>
                                    <!-- <td>
                                        <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                            <input type="checkbox" class="custom-control-input" id="customCheckBox2" required="">
                                            <label class="custom-control-label" for="customCheckBox2"></label>
                                        </div>
                                    </td> -->
                                    <td><strong>{{ $dish->id }}</strong></td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <img src="{{ Storage::disk('public')->url($dish->image) }}" class="rounded-lg mr-2" width="24" alt="" />
                                            <a href="{{ route('admin.dish.edit',['id' => $dish->id ]) }}">
                                                <span class="w-space-no">{{ $dish->name }}</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>{{ $dish->weight }} {{ $dish->unit->name }}</td>
                                    <td>{{ $dish->price }}</td>
                                    <td>
                                        <div class="d-flex align-items-center"><i class="fa fa-circle text-success mr-1"></i> Successful</div>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('admin.dish.edit', ['id' => $dish->id]) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-danger shadow btn-xs sharp delete-dish" data-dish-id="{{ $dish->id }}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete-dish">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.dish.delete') }}" method="POST" id="form-delete-dish">
                @csrf
                <input type="hidden" name="dishId">
                <div class="modal-header">
                    <h5 class="modal-title">Удаление блюда</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">Вы действительно хотите удалить это блюдо?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary light" data-dismiss="modal">Нет</button>
                    <button type="submit" class="btn btn-danger">Удалить</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
    $('.delete-dish').on('click', function(e) {
        e.preventDefault();
        let modal = $('#modal-delete-dish');
        let dishId = $(this).data('dish-id');
        let form = $('#form-delete-dish');

        form.find("[name='dishId']").val(dishId);

        modal.modal('show');
    });
</script>

@endsection