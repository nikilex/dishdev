@extends('admin.layouts.app')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/cropper.min.css') }}">
@endsection

@section('content')
<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <!-- <div class="welcome-text">
                <h4>Добавление блюда!</h4>
                <span>Element</span>
            </div> -->
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Добавление блюда</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Данные блюда</h4>
                </div>
                <div class="card-body">
                    <div class="basic-form">
                        <form method="post" id="dish-form" action="{{ route('admin.dish.store') }}" enctype="multipart/form-data">
                            @csrf
                            <!-- <input type="file" name="" id="zdd"> -->
                            <input type="file" class="form-control d-none" name="full-image" id="addImages">
                            <input type="hidden" class="form-control d-none" id="dataImg" name="image">
                            <input type="hidden" name="top">
                            <input type="hidden" name="left">
                            <input type="hidden" name="width">
                            <input type="hidden" name="height">
                            <div class="form-group col">
                                <label>Название блюда</label>
                                <input type="text" class="form-control" name="name" value="" required>
                            </div>
                            <div class="form-group col">
                                <label>Категория</label>
                                <select class="form-control" name="category" id="category" required>
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label>Вес</label>
                                <input type="text" class="form-control" name="weight" value="" requiredd>
                            </div>
                            <div class="form-group col">
                                <label>Единица измерения</label>
                                <select class="form-control" name="unit" id="unit" required>
                                    @foreach($units as $unit)
                                    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label>Цена</label>
                                <input type="text" class="form-control" name="price" value='' required>
                            </div>
                            <div class="form-group col">
                                <label>Состав блюда</label>
                                <textarea class="form-control" name="description" rows="4" id="comment" required></textarea>
                            </div>
                            <div class="form-group col">
                                <button type="submit" id="addDish" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Изображение</h4>
                </div>
                <div class="card-body">
                    <div class="newImg" class="w-100">
                    </div>
                    <img src="" id="newImg" alt="">
                    <div class="addImage">
                    </div>
                    <div class="button-block mt-2 d-none">
                        <button class="btn btn-primary" id="editImg">Изменить</button>
                        <button class="btn btn-danger" id="deleteImg">Удалить</button>
                    </div>
                </div>

                <!-- <div class="container">
                    <h1>Upload cropped image to server</h1>
                    <label class="label" data-toggle="tooltip" title="Change your avatar">
                        <img class="rounded" id="avatar" src="https://avatars0.githubusercontent.com/u/3456749?s=160" alt="avatar">
                        <input type="file" class="sr-only" id="input" name="image" accept="image/*">
                    </label>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                    </div>
                    <div class="alert" role="alert"></div>
                    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalLabel">Crop the image</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="img-container">
                                        <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-primary" id="crop">Crop</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->


            </div>
        </div>
        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">Обрезка изображения</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <img id="image" src="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <button type="button" class="btn btn-primary" id="crop">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @section('scripts')
    <script src="{{ asset('js/cropper.min.js')}}"></script>
    <script>
        // $('.addImage, #editImg').on('click', function() {
        //     $('#addImages').trigger('click');
        // });

        // $('#deleteImg').on('click', function() {
        //     $('#addImages').val('');
        //     $('.newImg').find('#newImg').remove();
        //     $('.addImage').removeClass('d-none');
        //     $('#dataImg').val('');
        //     $('.button-block').addClass('d-none');
        // });

        // window.addEventListener('DOMContentLoaded', function() {
        //     var image = document.getElementById('image');
        //     var input = document.getElementById('addImages');

        //     var $modal = $('#modal');
        //     var cropper;

        //     let dishForm = $('#dish-form');
        //     let x, y, width, height;

        //     $('[data-toggle="tooltip"]').tooltip();

        //     input.addEventListener('change', function(e) {
        //         var files = e.target.files;
        //         var done = function(url) {
        //             input.value = '';
        //             image.src = url;

        //             $modal.modal('show');
        //         };
        //         var reader;
        //         var file;
        //         var url;

        //         if (files && files.length > 0) {
        //             file = files[0];

        //             if (URL) {
        //                 done(URL.createObjectURL(file));
        //             } else if (FileReader) {
        //                 reader = new FileReader();
        //                 reader.onload = function(e) {
        //                     done(reader.result);
        //                 };
        //                 reader.readAsDataURL(file);
        //             }
        //         }
        //     });

        //     $modal.on('shown.bs.modal', function() {
        //         cropper = new Cropper(image, {
        //             aspectRatio: 16 / 9,
        //             zoomable: false,
        //             crop(event) {
        //                 // console.log(event.detail.x);
        //                 // console.log(event.detail.y);
        //                 // console.log(event.detail.width);
        //                 // console.log(event.detail.height);
        //                 // console.log(event.detail.rotate);
        //                 // console.log(event.detail.scaleX);
        //                 // console.log(event.detail.scaleY);
        //                 x = event.detail.x;
        //                 y = event.detail.y;
        //                 width = event.detail.width;
        //                 height = event.detail.height;
        //             },
        //         });
        //     }).on('hidden.bs.modal', function() {
        //         cropper.destroy();
        //         cropper = null;
        //     });

        //     document.getElementById('crop').addEventListener('click', function() {
        //         var canvas;

        //         $modal.modal('hide');

        //         if (cropper) {
        //             canvas = cropper.getCroppedCanvas();

        //             // Устанавливаем изображение
        //             $('.newImg').html($('<img>', {
        //                 id: 'newImg',
        //                 src: canvas.toDataURL(),
        //             }));

        //             $('.button-block').removeClass('d-none');
        //             $('.addImage').addClass('d-none');

        //             $('#dataImg').val(canvas.toDataURL());

        //             dishForm.find('[name=detailX]').val(x);
        //             dishForm.find('[name=detailY]').val(y);
        //             dishForm.find('[name=width]').val(width);
        //             dishForm.find('[name=height]').val(height);


        //             canvas.toBlob(function(blob) {

        //                 var formData = new FormData();

        //                 formData.append('avatar', blob, 'avatar.jpg');
        //             });
        //         }
        //     });
        // });
    </script>

    <!-- <script>
        $('.addImage, #editImg').on('click', function() {
            $('#addImages').trigger('click');
        });

        var $modal = $('#modal');
        var $alert = $('.alert');
       // var input = document.getElementById('addImages');

        $('#addImages').on('change', function(e) {
            var image = $('#image');

            var files = e.target.files;
            var done = function(url) {
               // input.value = '';
                image.src = url;
                $alert.hide();
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }

        });

        $modal.on('shown.bs.modal', function() {
            cropper = new Cropper(image, {
                aspectRatio: 16 / 9,
                zoomable: false,
                // crop(event) {
                //     console.log(event.detail.x);
                //     console.log(event.detail.y);
                //     console.log(event.detail.width);
                //     console.log(event.detail.height);
                //     console.log(event.detail.rotate);
                //     console.log(event.detail.scaleX);
                //     console.log(event.detail.scaleY);
                // },
            });
        }).on('hidden.bs.modal', function() {
            cropper.destroy();
            cropper = null;
        });
    </script> -->

    <script>
        $('.addImage, #editImg').on('click', function() {
            $('#addImages').trigger('click');
        });

        $('#deleteImg').on('click', function() {
            $('#addImages').val('');
            $('#newImg').attr('src', '');
            $('.addImage').show();
            $('#dataImg').val('');
            $('.button-block').addClass('d-none');
        });

        window.addEventListener('DOMContentLoaded', function() {
            // var avatar = document.getElementById('avatar');
            var avatar = document.getElementById('newImg');

            var image = document.getElementById('image');
            var input = document.getElementById('addImages');
            // var input = document.getElementById('input');
            var $progress = $('.progress');
            var $progressBar = $('.progress-bar');
            var $alert = $('.alert');
            var $modal = $('#modal');
            var formData = new FormData();
            var cropper;

            $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function(e) {
                var files = e.target.files;
                var done = function(url) {
                    //  input.value = '';
                    image.src = url;
                    $alert.hide();
                    $modal.modal('show');
                };
                var reader;
                var file;
                var url;

                if (files && files.length > 0) {
                    file = files[0];

                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function(e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });

            $modal.on('shown.bs.modal', function() {
                cropper = new Cropper(image, {
                    aspectRatio: 16 / 9,
                    zoomable: false,
                    crop(event) {
                        let top = $('[name="top"]');
                        let left = $('[name="left"]');
                        let width = $('[name="width"]');
                        let height = $('[name="height"]');

                        top.val(event.detail.y);
                        left.val(event.detail.x);
                        width.val(event.detail.width);
                        height.val(event.detail.height);
                    },
                });
            }).on('hidden.bs.modal', function() {
                cropper.destroy();
                cropper = null;
            });

            document.getElementById('crop').addEventListener('click', function() {
                var initialAvatarURL;
                var canvas;
                var avatarImg;
                let boxAddImg = $('.addImage');
                let buttonBox = $('.button-block');


                $modal.modal('hide');
                boxAddImg.hide();
                buttonBox.removeClass('d-none');

                if (cropper) {
                    canvas = cropper.getCroppedCanvas(
                        //     {
                        //     width: 160,
                        //     height: 160,
                        // }
                    );
                    initialAvatarURL = avatar.src;
                    avatar.src = canvas.toDataURL();
                    $progress.show();
                    $alert.removeClass('alert-success alert-warning');
                    canvas.toBlob(function(blob) {
                        avatarImg = blob;
                        formData.append('avatar', blob, 'avatar.jpg');
                        formData.append('_token', $('meta[name="_token"]').attr('content'));
                        // console.log(blob);
                        // document.querySelector('#zdd').files = blob;
                        
                        // $.post("{{ route('admin.dish.store') }}", {
                        //     '_token': $('meta[name="_token"]').attr('content'),
                        //     'avatar': blob
                        // }, function(res) {
                        //     console.log(res);
                        // })
                    });
                }


            });



            // $('#addDish').on('click', function (e) {
            //     e.preventDefault();

            //     $.ajax('/admin/dish/store', {
            //                 method: 'POST',
            //                 data: formData,
            //                 processData: false,
            //                 contentType: false,

            //                 success: function() {
            //                     $alert.show().addClass('alert-success').text('Upload success');
            //                 },

            //                 error: function() {
            //                     avatar.src = initialAvatarURL;
            //                     $alert.show().addClass('alert-warning').text('Upload error');
            //                 },

            //                 complete: function() {
            //                     $progress.hide();
            //                 },
            //             });
            // })
        });
    </script>
    @endsection