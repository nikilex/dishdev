        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="deznav">
            <div class="deznav-scroll">
                <div class="add-menu-sidebar">
                    <img src="/images/icon1.png" alt="" />
                    <p>Организуйте свое меню с помощью кнопки ниже</p>
                    <a href="{{ route('admin.dish.add') }}" class="btn btn-primary btn-block light">+ Добавить блюдо</a>
                </div>
                <ul class="metismenu" id="menu">
                    <li><a class="has-arrow ai-icon" href="{{ route('admin.index') }}" aria-expanded="false">
                            <i class="flaticon-381-television"></i>
                            <span class="nav-text">Рабочий стол</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="{{ route('admin.categories.index') }}" aria-expanded="false">
                            <i class="flaticon-381-app"></i>
                            <span class="nav-text">Категории</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="{{ route('admin.dish.index') }}" aria-expanded="false">
                            <i class="flaticon-381-notepad-2"></i>
                            <span class="nav-text">Меню</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="{{ route('admin.orders.index') }}" aria-expanded="false">
                            <i class="flaticon-381-ring"></i>
                            <span class="nav-text">Заказы</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="{{ route('admin.settings.index') }}" aria-expanded="false">
                            <i class="flaticon-381-settings-6"></i>
                            <span class="nav-text">Настройки</span>
                        </a>
                    </li>
                    <li><a class="has-arrow ai-icon" href="{{ route('logout') }}" aria-expanded="false" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                            <i class="flaticon-381-exit-2"></i>
                            <span class="nav-text">Выход</span>
                        </a>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>

            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->