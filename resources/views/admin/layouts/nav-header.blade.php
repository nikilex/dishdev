        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <a href="/" class="brand-logo">
                <img class="logo-abbr" src="/images/logo.png" alt="">
               <span style="font-size: 32px; font-weight: 700; font-family: Roboto; color: #000; margin-left: 10px"> DISHBOT</span>
                <!-- <img class="logo-compact" src="/images/logo-text.png" alt="">
                <img class="brand-title" src="/images/logo-text.png" alt=""> -->
            </a>

            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->