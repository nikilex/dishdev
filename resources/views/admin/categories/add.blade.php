@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Добавление категории</a></li>
                </ol>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Данные категории</h4>
                </div>
                <div class="card-body">
                    <div class="basic-form">
                        <form method="post" id="dish-form" action="{{ route('admin.categories.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group col">
                                <label>Название категории</label>
                                <input type="text" class="form-control" name="name" value="" required>
                            </div>
                            <div class="form-group col">
                                <button type="submit" id="addDish" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @section('scripts')

    @endsection