@extends('admin.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <div class="welcome-text">
                <h4>Категории</h4>
            </div>
        </div>
        <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Админпанель</a></li>
                <li class="breadcrumb-item active"><a href="javascript:void(0)">Категории</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Таблица категорий</h4>
                    <div class="text-right">
                        <a href="{{ route('admin.categories.create') }}">Добавить категорию</a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-responsive-md">
                            <thead>
                                <tr>
                                    <!-- <th style="width:50px;">
                                        <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                            <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                            <label class="custom-control-label" for="checkAll"></label>
                                        </div>
                                    </th> -->
                                    <th><strong>№</strong></th>
                                    <th><strong>Наименование</strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                <tr>
                                    <!-- <td>
                                        <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                            <input type="checkbox" class="custom-control-input" id="customCheckBox2" required="">
                                            <label class="custom-control-label" for="customCheckBox2"></label>
                                        </div>
                                    </td> -->
                                    <td><strong>{{ $category->id }}</strong></td>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            <a href="{{ route('admin.categories.edit', $category->id) }}">
                                                <span class="w-space-no">{{ $category->name }}</span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                            <a href="#" class="btn btn-danger shadow btn-xs sharp delete-category" data-category-id="{{ $category->id }}"><i class="fa fa-trash"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete-category">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST" id="form-delete-category">
                @csrf
                @method('DELETE')
                <input type="hidden" name="categoryId">
                <div class="modal-header">
                    <h5 class="modal-title">Удаление категории</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">Вы действительно хотите удалить эту категорию?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary light" data-dismiss="modal">Нет</button>
                    <button type="submit" class="btn btn-danger">Удалить</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
    $('.delete-category').on('click', function(e) {
        e.preventDefault();
        let modal = $('#modal-delete-category');
        let categoryId = $(this).data('category-id');
        let form = $('#form-delete-category');
        let url = '{{ route("admin.categories.destroy", ":id") }}'
        url = url.replace(':id', categoryId);

        form.find("[name='categoryId']").val(categoryId);
        form.attr('action', url)

        modal.modal('show');
    });
</script>

@endsection