<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DishSetOfDish;

class DishSetOfDishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DishSetOfDish::create([
            'dish_id'        => 1,
            'set_of_dish_id' => 1,
        ]);

        DishSetOfDish::create([
            'dish_id'        => 2,
            'set_of_dish_id' => 1,
        ]);

        DishSetOfDish::create([
            'dish_id'        => 3,
            'set_of_dish_id' => 1,
        ]);
    }
}
