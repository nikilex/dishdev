<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SetOfDish;

class SetOfDishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SetOfDish::create([
            'name'    => 'Бизнес ланч №1',
            'price'   => '250',
            'menu_id' => 1
        ]);

        SetOfDish::create([
            'name'  => 'Бизнес ланч №2',
            'price' => '300',
            'menu_id' => 1
        ]);

        SetOfDish::create([
            'name'  => 'Бизнес ланч №3',
            'price' => '100',
            'menu_id' => 1
        ]);
    }
}
