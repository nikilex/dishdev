<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'       => 'admin',
            'email'      => 'admin',
            'company_id' => 1,
            'password'   => Hash::make('admin'),
        ]);

        User::create([
            'name'       => 'user',
            'email'      => 'user',
            'company_id' => 2,
            'password'   => Hash::make('user'),
        ]);
    }
}
