<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create(
            [
                'name' => 'Новый',
                'slug' => 'new'
            ]);
        Status::create(
            [
                'name' => 'В работе',
                'slug' => 'work'
            ]);

        Status::create(
            [
                'name' => 'Отправлен на доставку',
                'slug' => 'delivery'
            ]);
        Status::create(  
            [
                'name' => 'Доставлен',
                'slug' => 'delivered'
            ]);
    }
}
