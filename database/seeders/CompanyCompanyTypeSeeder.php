<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyCompanyType;

class CompanyCompanyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyCompanyType::create([
            'company_id'      => 1,
            'company_type_id' => 1
        ]);

        CompanyCompanyType::create([
            'company_id'      => 2,
            'company_type_id' => 2
        ]);
    }
}
