<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name'   => 'Appetitus',
            'city_id' => 1,
        ]);

        Company::create([
            'name'   => 'Престиж',
            'city_id' => 1,
        ]);

        Company::create([
            'name'   => 'Шашлычок',
            'city_id' => 1,
        ]);

        Company::create([
            'name'   => '9 фонарей',
            'city_id' => 1,
        ]);

        Company::create([
            'name'   => 'BURGER ENERGY',
            'city_id' => 1,
        ]);
    }
}
