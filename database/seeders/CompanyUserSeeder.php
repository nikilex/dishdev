<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyUser;
use Illuminate\Support\Facades\Hash;

class CompanyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyUser::create([
            'company_id' => 1,
            'user_id'    => 1,
            'password'   => Hash::make('admin'),
            'role_id'    => 1,
        ]);

        CompanyUser::create([
            'company_id' => 1,
            'user_id'    => 2,
            'password'   => Hash::make('user'),
            'role_id'    => 2,
        ]);
    }
}
