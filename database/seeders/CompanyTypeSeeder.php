<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CompanyType;

class CompanyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'Производство питания',
            'Организация'
        ];
        foreach($arr as $row)
        {
            CompanyType::create([
                'name' => $row
            ]);
        }
    }
}
