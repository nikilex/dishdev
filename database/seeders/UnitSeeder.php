<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Unit;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'гр',
            'кг',
            'мл'
        ];
        
        foreach($arr as $row)
        {
            Unit::create([
                'name' => $row
            ]);
        }
    }
}
