<?php

namespace Database\Seeders;

use App\Models\FileTypes;
use Illuminate\Database\Seeder;

class FileTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileTypes::create([
            'name' => 'Изображение',
            'slug' => 'img',
        ]);

        FileTypes::create([
            'name' => 'Файл',
            'slug' => 'file',
        ]);
    }
}
