<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            CitySeeder::class,
            CompanySeeder::class,
            CompanyTypeSeeder::class,
            CompanyCompanyTypeSeeder::class,

            RoleSeeder::class,
            UserSeeder::class,
            CompanyUserSeeder::class,

            UnitSeeder::class,
            // MenuSeeder::class,
           //  SetOfDishSeeder::class,
            CategorySeeder::class,
            FileTypesSeeder::class,
            DishSeeder::class,
            StatusesSeeder::class,
            
           // DishSetOfDishSeeder::class,
        ]);
    }
}
