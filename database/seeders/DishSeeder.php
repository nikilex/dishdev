<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dish;

class DishSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dish::create([
            'name'        => 'Пельмени',
            'weight'      => '300',
            'description' => 'Состав: мясо, тесто. Сочные пельмешки с изюминкой!',
            'price'       => 350,
            'category_id' => 1,
            'company_id'  => 1,
            'unit_id'     => 1
        ]);

        Dish::create([
            'name'        => 'Салат "Оливье',
            'weight'      => '150',
            'description' => 'Состав: мясо, тесто. Сочные пельмешки с изюминкой!',
            'price'       => 150,
            'category_id' => 4,
            'company_id'  => 1,
            'unit_id'     => 1
        ]);

        Dish::create([
            'name'        => 'Роллы',
            'weight'      => '500',
            'description' => 'Состав: мясо, тесто. Сочные пельмешки с изюминкой!',
            'price'       => 750,
            'category_id' => 2,
            'company_id'  => 1,
            'unit_id'     => 1
        ]);
    }
}
