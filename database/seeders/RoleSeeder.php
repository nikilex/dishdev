<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'admin',
            'user'
        ];
        
        foreach($arr as $row)
        {
            Role::create([
                'name' => $row
            ]);
        }
    }
}
