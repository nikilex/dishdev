<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Company;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::first();

        Category::create([
            'name' => 'Обед',
            'company_id' => $company->id,
        ]);

        Category::create([
            'name' => 'Роллы',
            'company_id' => $company->id,
        ]);

        Category::create([
            'name' => 'Напитки',
            'company_id' => $company->id,
        ]);

        Category::create([
            'name' => 'Салаты',
            'company_id' => $company->id,
        ]);
    }
}
