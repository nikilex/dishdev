<?php

namespace App\Repositories\Eloquent\Criteria;


use App\Bases\Repository\Eloquent\Contracts\CriterionInterface;

class Published implements CriterionInterface
{

    public function apply($model)
    {
        return $model->wherePublished(1);
    }
}
