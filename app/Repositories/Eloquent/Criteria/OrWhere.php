<?php


namespace App\Repositories\Eloquent\Criteria;


use App\Bases\Repository\Eloquent\Contracts\CriterionInterface;

class OrWhere implements CriterionInterface
{

    protected array $param;

    public function __construct($param)
    {
        $this->param = $param;
    }

    public function apply($model)
    {
        return $model->orWhere($this->param);
    }
}
