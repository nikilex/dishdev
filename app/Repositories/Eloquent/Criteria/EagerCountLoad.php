<?php


namespace App\Repositories\Eloquent\Criteria;


use App\Bases\Repository\Eloquent\Contracts\CriterionInterface;

class EagerCountLoad implements CriterionInterface
{

    protected array $relationships;

    public function __construct($relationships)
    {
        $this->relationships = $relationships;
    }

    public function apply($model)
    {
        return $model->withCount($this->relationships);
    }
}
