<?php

namespace App\Repositories\Eloquent\Criteria;


use App\Bases\Repository\Eloquent\Contracts\CriterionInterface;

class LatestFirst implements CriterionInterface
{

    public function apply($model)
    {
        return $model->latest();
    }
}
