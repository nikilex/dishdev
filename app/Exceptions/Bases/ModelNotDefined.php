<?php

namespace App\Exceptions\Bases;

use Exception;

class ModelNotDefined extends Exception
{
}
