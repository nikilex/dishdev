<?php

namespace App\Exceptions\Bases;

use Exception;

class MethodNotDefined extends Exception
{
}
