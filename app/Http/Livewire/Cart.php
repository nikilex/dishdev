<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Cart extends Component
{
    public $count;
    public $plus;
    public $session;

    public function render()
    {
        $this->session = session('plus');
        $this->count = $this->plus;
        return view('livewire.cart');
    }

    public function increment()
    {
        session(['plus' => $this->plus]);
        $this->count = $this->plus;
    }
}
