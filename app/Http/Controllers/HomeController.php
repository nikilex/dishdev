<?php

namespace App\Http\Controllers;

use App\Events\NewEvent;
use App\Events\OrderEvent;
use App\Models\Company;
use App\Models\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders  = Order::get();
        $company = Company::first();
        return view('home', compact('orders', 'company'));
    }

    public function eve(Request $request)
    {
        //NewEvent::dispatch($request);
        //event(new NewEvent('lex'));
        $order = Order::create([
            'address'   => 'Карпинск',
            'phone'     => '+79920039963',
            'comment'   => 'Горчички много',
            'status_id' => 1
        ]);

        $company = Company::first();
        
        OrderEvent::dispatch($order, $company);
        
        return $order;
    }
}
