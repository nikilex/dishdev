<?php

namespace App\Http\Controllers\API\v1\Cart;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dishes\DishResource;
use App\Http\Resources\TelegramCart\TelegramCartCollection;
use App\Models\CartTelegramChat;
use App\Models\Dish;
use Illuminate\Http\Request;

class CartTelegramChatController extends Controller
{


    public function index(Request $request)
    {
        return CartTelegramChat::where('telegram_chat_id', $request->tci)->with('dish')->get();
    }

    public function store(Request $request)
    {
        $beforeCart = CartTelegramChat::where('telegram_chat_id', $request->tci)
            ->where('dish_id', $request->di)
            ->first();

        $dish = Dish::find($request->di);

        if ($beforeCart) {
            $createdCartItem = CartTelegramChat::where('telegram_chat_id', $request->tci)
                ->where('dish_id', $request->di)
                ->update([
                    'count' => $beforeCart->count + 1
                ]);
        } else {
            $createdCartItem = CartTelegramChat::create([
                'telegram_chat_id' => $request->tci,
                'dish_id'          => $request->di,
                'count'            => 1,
                'price'            => $dish->price
            ]);
        }

        return [
            'createdCartItem' => $createdCartItem,
            'dish'            => new DishResource($dish)
        ];
    }

    public function changeCount(Request $request)
    {
        $cart = CartTelegramChat::where('telegram_chat_id', $request->tci)
            ->where('dish_id', $request->di)
            ->first();

        if ($request->m == 'p') {
            $cart->update([
                'count' => $cart->count + 1
            ]);
        }

        if ($request->m == 'm') {
            if ($cart->count <= 1) {
                $cart->delete();
            } else {
                $cart->update([
                    'count' => $cart->count - 1
                ]);
            }
        }
    }

    public function getTelegramCart(Request $request)
    {
        $cart = CartTelegramChat::where('telegram_chat_id', $request->tci)
            ->with('dish')
            ->get();

        return [
            'cart'  => $cart,
            'total' => $cart->map(function ($product, $key) {
                return $product->price * $product->count;
            })->sum()
        ];
    }

    public function clearTelegramCart(Request $request)
    {
        return CartTelegramChat::where('telegram_chat_id', $request->tci)
            ->delete();
    }

    public function update()
    {
    }

    public function delete()
    {
    }
}
