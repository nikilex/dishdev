<?php

namespace App\Http\Controllers\API\v1\Dishes;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dishes\DishCollection;
use App\Http\Resources\Dishes\DishResource;
use App\Models\Dish;
use Illuminate\Http\Request;
use App\Services\Dish\DishService;
use Illuminate\Support\Facades\Auth;

class DishesController extends Controller
{
    /**
     * @var DishService
     */
    protected $dishService;

    public function __construct(
        DishService $dishService
    ) {
        $this->dishService = $dishService;
    }

    public function index()
    {
        $dishes = $this->dishService->getCompanyDishes(1);
        return new DishCollection($dishes);
    }

    public function getByCategory(Request $request)
    {
        $perPage = (int) $request->per_page ?: 15;
        $dishes = $this->dishService->getByCategory($request->ci, $perPage);
        return new DishCollection($dishes);
    }

    public function getById(Request $request)
    {
        $dish = Dish::where('id', $request->di)->with('cart', function($q) use ($request) {
            $q->where('telegram_chat_id', $request->tci)
            ->where('dish_id', $request->di);
        })->find($request->di);
        return new DishResource($dish);
    }

    public function store(Request $request)
    {
       
    }

    public function update(Request $request)
    {
       
    }

    public function delete(Request $request)
    {
        
    }
}
