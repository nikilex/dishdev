<?php

namespace App\Http\Controllers\API\v1\Categories;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CategoryListRequest;
use App\Http\Resources\Categories\CategoryCollection;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    public function __construct(
        CategoryService $categoryService
    ) {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $validated = [
            'company_id' => $request->company_id,
            'per_page'   => $request->per_page ? $request->per_page : 15
        ];

        $categories = $this->categoryService->paginate($validated);
        return new CategoryCollection($categories);
    }

    public function store()
    {

    }

    public function update()
    {

    }

    public function delete()
    {
        
    }
}
