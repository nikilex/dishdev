<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\SetOfDish;
use App\Models\Dish;
use App\Services\City\CityService;

class IndexController extends Controller
{
    /**
     * @var CityService
     */
    protected CityService $cityService;

    public function __construct(
        CityService $cityService
    ) {
        $this->cityService = $cityService;
    }

    public function index()
    {
        $companies = Company::with('city')->get();

        return view('index', compact('companies'));
    }

    public function company($id)
    {
        $company = Company::with('dishes.unit')
        ->where('id', $id)
        ->first();

        return view('company', compact('company'));
    }

    public function companies()
    {
        $companies = Company::with('city')->get();

        return view('companies', compact('companies'));
    }

    public function addDishInSet(Request $request)
    {
        SetOfDish::find($request->set_of_dish_id)->dishes()->attach($request->dish_id);
        return redirect('/menu');
    }

    public function city()
    {
        $cities = $this->cityService->first();
        dd($cities);
    }
}
