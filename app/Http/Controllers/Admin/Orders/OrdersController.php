<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Events\OrderEvent;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Order\OrderService;

class OrdersController extends Controller
{
    protected OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function index()
    {
        $user    = Auth::user();
        $orders  = Order::where('company_id', $user->company_id)->get();
        $company = Company::where('id', $user->company_id)->first();
        return view('admin.orders.index', compact('orders', 'company'));
    }

    /**
     * Страница заказа
     * 
     * @param  Illuminate\Http\Request
     * @return view()
     */
    public function order(Request $request)
    {
        $orderId = $request->orderId;
        $order   = $this->orderService->getOrder($orderId);

        return view('admin.orders.order', compact('order'));
    }

    public function store(Request $request)
    {
        $user    = Auth::user();
        $company = Company::where('id', $request->cart['rst_id'])->first();

        $order = Order::create([
            'address'    => $request->address,
            'phone'      => $request->phone,
            'comment'    => $request->comment,
            'status_id'  => 1,
            'company_id' => $request->cart['rst_id']
        ]);

        foreach ($request->cart['dishes'] as $dish) {
            $order->dishes()->attach($dish['id'], ['price' => $dish['price'], 'count' => $dish['count']]);
        } 
        
        OrderEvent::dispatch($order, $company);
        
        return $order;
    }
}
