<?php

namespace App\Http\Controllers\Admin\Dishes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unit;
use App\Models\Category;
use App\Models\Dish;
use App\Services\Dish\DishService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DishesController extends Controller
{
    /**
     * @var DishService
     */
    protected $dishService;

    public function __construct(
        DishService $dishService
    ) {
        $this->dishService = $dishService;
    }

    public function index()
    {
        $dishes = $this->dishService->getCompanyDishes(Auth::user()->company_id);
        return view('admin.dish.index', compact('dishes'));
    }

    /**
     * Возвращает блюдо по идентификатору
     * 
     * @param Request
     * 
     * @return
     */
    public function getDishById(Request $request)
    {
        return $this->dishService->getDishById($request->dishId);
    }

    public function add()
    {
        $categories = Category::get();
        $units      = Unit::get();

        return view('admin.dish.add', compact('categories', 'units'));
    }

    public function edit($id)
    {
        $categories = Category::get();
        $units      = Unit::get();
        $dish       = Dish::find($id);

        return view('admin.dish.edit', compact('categories', 'units', 'dish'));
    }

    public function store(Request $request)
    {
        $dish = $this->dishService->storeDish($request->toArray());
        return redirect(route('admin.dish.index'));
    }

    public function update(Request $request)
    {
        $updateDish = $this->dishService->updateDish($request->id, $request->toArray());
        return redirect(route('admin.dish.index'));
    }

    public function delete(Request $request)
    {
        $deleteDish = $this->dishService->deleteDish($request->dishId);
        return redirect(route('admin.dish.index'));
    }
}
