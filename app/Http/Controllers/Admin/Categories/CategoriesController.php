<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\Category\CategoryService;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    public function __construct(
        CategoryService $categoryService
    ) {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->getAll();

        return view('admin.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.add');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function store(Request $request)
    {
        Category::create([
            'name' => $request->name,
            'company_id' => auth()->user()->company_id
        ]);

        return redirect(route('admin.categories.index'));
    }

    public function update($id, Request $request)
    {
        $category = Category::where('id', $id)->update([
            'name' => $request->name
        ]);

        return redirect(route('admin.categories.index'));
    }

    public function destroy($id)
    {
        $category = Category::find($id);

        $category->delete();

        return redirect(route('admin.categories.index'));
    }
}
