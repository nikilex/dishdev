<?php

namespace App\Http\Controllers\Restaurant;

use App\Events\ChatMessage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Comment;
use App\Services\Company\CompanyService;

class RestaurantController extends Controller
{
    /**
     * @var CompanyService
     */
    protected CompanyService $companyService;

    public function __construct(
        CompanyService $companyService
    ) {
        $this->companyService = $companyService;
    }
    public function index($id)
    {
        $restaurant = $this->companyService->getWithCompanyAndDishesById($id);

        return view('restaurant.index', compact('restaurant'));
    }

    public function comment(Request $request)
    {
        /**
         * Валидация. Добавляю сообщение в базу,
         * получаю модель Comment $comment с сообщением
         */
        $comment = 'nikilex';
       // ChatMessage::dispatch($request->All());
        //return $request->All();

        event(new ChatMessage($comment)); // Это для примера. Отправка сообщения всем активным пользователям канала
       // broadcast(new ChatMessage($comment))->toOthers(); // Отправляю сообщение всем, кроме текущего пользователя
    }
}
