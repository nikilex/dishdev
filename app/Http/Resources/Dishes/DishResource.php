<?php

namespace App\Http\Resources\Dishes;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class DishResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => $this->category_id,
            'unit_id' => $this->unit_id,
            'description' => $this->description,
            'weight' => $this->weight,
            'price' => $this->price,
            'cart'  => $this->cart,
            'image' => Storage::disk('public')->has($this->image) ?  Storage::disk('public')->url($this->image) : asset('img/basket.jpg'),
            'image_full_path' => Storage::disk('public')->has($this->image) ?  Storage::disk('public')->path($this->image) : asset('img/basket.jpg'),
            'company_id' => $this->company_id
        ];
    }
}
