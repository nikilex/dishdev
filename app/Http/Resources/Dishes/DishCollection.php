<?php

namespace App\Http\Resources\Dishes;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class DishCollection
 * @package App\Billing\Phone\Http\Resources
 */
class DishCollection extends ResourceCollection
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
        ];
    }
}
