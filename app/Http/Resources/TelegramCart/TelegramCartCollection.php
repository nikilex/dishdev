<?php

namespace App\Http\Resources\TelegramCart;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * Class TelegramCartCollection
 * @package App\Billing\Phone\Http\Resources
 */
class TelegramCartCollection extends ResourceCollection
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data' => $this->collection,
        ];
    }
}
