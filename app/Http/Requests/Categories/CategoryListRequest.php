<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;

class CategoryListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => ['required', 'exists:App\Models\Categories,id']
        ];
    }

    public function messages()
    {
        return [
            'company_id.required' => 'Отсутствует идентификатор категории',
            'company_id.exists'   => 'Категория отсутствует в БД',
        ];
    }
}
