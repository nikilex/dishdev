<?php

namespace App\Providers;

use App\Services\City\Repositories\Contracts\CityRepository;
use App\Services\City\Repositories\EloquentCityRepository;
use App\Services\Dish\Repositories\Contracts\DishRepository;
use App\Services\Dish\Repositories\EloquentDishRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CityRepository::class, EloquentCityRepository::class);
        $this->app->bind(DishRepository::class, EloquentDishRepository::class);
    }
}

