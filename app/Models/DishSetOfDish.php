<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DishSetOfDish extends Model
{
    use HasFactory;

    protected $table = "dish_set_of_dish";
}
