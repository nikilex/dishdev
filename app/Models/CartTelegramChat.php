<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartTelegramChat extends Model
{
    use HasFactory;

    protected $fillable = [
        'telegram_chat_id',
        'dish_id',
        'price',
        'count'
    ];

    public function dish()
    {
        return $this->belongsTo(Dish::class);
    }
}
