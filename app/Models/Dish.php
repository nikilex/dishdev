<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id',
        'unit_id',
        'description',
        'weight',
        'price',
        'image',
        'company_id'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function cart()
    {
        return $this->hasMany(CartTelegramChat::class);
    }
}
