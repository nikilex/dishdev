<?php

namespace App\Bases\Repository\Eloquent;

use App\Bases\Repository\Eloquent\Contracts\BaseRepositoryInterface;
use App\Bases\Repository\Eloquent\Contracts\RepositoryCriteriaInterface;
use App\Exceptions\Bases\ModelNotDefined;
use Illuminate\Support\Arr;

abstract class BaseRepository implements BaseRepositoryInterface, RepositoryCriteriaInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = $this->getModelClass();
    }

    protected function getModelClass()
    {
        if (!method_exists($this, 'model')) {
            throw new ModelNotDefined();
        }

        return app()->make($this->model());
    }

    /**
     * Возвращает экземпляр модели по ее идентификатору
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->find($id);
    }


    /**
     * Извлекает все результат build'ера
     * @return mixed
     */
    public function all()
    {
        return $this->model->get();
    }

    /**
     * Извлекает первый результат build'ера
     * @return mixed
     */
    public function first()
    {
        return $this->model->first();
    }

    /**
     * Сортирует результат build'ера
     * @return mixed
     */
    public function orderBy($name, $method)
    {
        return $this->model->orderBy($name, $method);
    }

    /**
     * Создает экземпляр модели в базе данных
     * @param array $modelData
     * @return mixed
     */
    public function create(array $modelData)
    {
        return $this->model->create($modelData);
    }

    /**
     * Удаляет модель по Id
     * @param int $modelId
     * @return mixed
     */
    public function delete(int $modelId)
    {
        $row = $this->find($modelId);
        return $row->delete();
    }

    /**
     * Обновляет данные внутри конкретной модели
     * @param       $modelId
     * @param array $updateData
     * @return mixed
     */
    public function update($modelId, array $updateData, bool $returnModel = false)
    {
        $row = $this->find($modelId);
        $update = $row->update($updateData);

        return $returnModel ? $row : $update;
    }

    /**
     * Возвращает пагинацию для модели
     * @param int $perPage
     * @return mixed
     */
    public function paginate(int $perPage)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Фильтрация модели
     * @param  mixed
     * @return mixed
     */
    public function where(...$criteria)
    {
        return $this->model->where(...$criteria);
    }

    /**
     * Фильтрация модели
     * @param  mixed
     * @return mixed
     */
    public function whereIn(...$criteria)
    {
        return $this->model->whereIn(...$criteria);
    }

    public function whereHas($relation, $contrains)
    {
        return $this->model->whereHas($relation, $contrains);
    }

     /**
     * Загрузка отношений методом жадной загрузки
     * @param  mixed
     * @return mixed
     */
    public function with(...$relationship)
    {
        return $this->model->with(...$relationship);
    }

    /**
     * Фильтрация модели с использованием критериев
     * @param mixed ...$criteria
     * @return $this
     */
    public function withCriteria(...$criteria)
    {
        $criteria = Arr::flatten($criteria);

        foreach ($criteria as $criterion) {
            $this->model = $criterion->apply($this->model);
        }

        return $this;
    }
}
