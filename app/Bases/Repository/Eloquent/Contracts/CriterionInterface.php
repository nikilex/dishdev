<?php

namespace App\Bases\Repository\Eloquent\Contracts;

interface CriterionInterface
{
    public function apply($model);
}
