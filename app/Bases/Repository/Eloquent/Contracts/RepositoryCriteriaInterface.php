<?php

namespace App\Bases\Repository\Eloquent\Contracts;

interface RepositoryCriteriaInterface
{
    public function withCriteria(...$criteria);
    public function where(...$criteria);
    public function with(...$relationship);
}
