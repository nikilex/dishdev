<?php

namespace App\Bases\Repository\Eloquent\Contracts;

interface BaseRepositoryInterface
{
    public function find($id);
    public function all();
    public function create(array $modelData);
    public function delete(int $modelId);
    public function paginate(int $perPage);
    public function update($modelId, array $updateData, bool $returnModel = false);
}
