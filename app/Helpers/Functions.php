<?php

use Illuminate\Support\Facades\Auth;

function getPathForDishImg()
{
    return Auth::id() . '/dish';
}

function getBytesFromHexString($hexdata)
{
    for ($count = 0; $count < strlen($hexdata); $count += 2)
        $bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

    return implode($bytes);
}

function decodeImageForPutStorage($base64String)
{
    $data = substr($base64String, strpos($base64String, ',') + 1);
    $data = base64_decode($data);

    return [
        'path' => getPathForDishImg() . '/dish_' . uniqid() . '.'. getImageMimeType($base64String),
        'data' => $data
    ];
}

function base64_encode_image($filename, $data) {
    //if ($filename) {
        //$imgbinary = fread(fopen($filename, "r"), filesize($filename));
        return 'data:image/' . getTypeByPath($filename) . ';base64,' . base64_encode($data);
   // }
}

function getTypeByPath($name)
{
    $tmp = explode('.', $name);
    return end($tmp);
}

function getImageMimeType($imagedata)
{
    $img = explode(',', $imagedata);
    $ini =substr($img[0], 11);
    $type = explode(';', $ini);

    return $type[0];

    // $imagemimetypes = array(
    //     "jpeg" => "FFD8",
    //     "png" => "89504E470D0A1A0A",
    //     "gif" => "474946",
    //     "bmp" => "424D",
    //     "tiff" => "4949",
    //     "tiff" => "4D4D"
    // );

    // foreach ($imagemimetypes as $mime => $hexbytes) {
    //     $bytes = getBytesFromHexString($hexbytes);
    //     if (substr($imagedata, 0, strlen($bytes)) == $bytes)
    //         return $mime;
    // }

    // return NULL;
}