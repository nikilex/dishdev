<?php

namespace App\Services\City;

use App\Services\City\Repositories\Contracts\CityRepository;
use App\Repositories\Eloquent\Criteria\Where;
use App\Repositories\Eloquent\Criteria\EagerLoad;
use App\Traits\BaseRepository;

class CityService
{
    use BaseRepository;

    /**
     * @var CityRepository
     */
    protected CityRepository $cityRepository;

    public function __construct(
        CityRepository    $cityRepository
    ) {
        $this->cityRepository = $cityRepository;
    }

    protected function getRepository()
    {
        return $this->cityRepository;
    }

}
