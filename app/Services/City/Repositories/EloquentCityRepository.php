<?php

namespace App\Services\City\Repositories;

use App\Bases\Repository\Eloquent\BaseRepository;
use App\Models\City;
use App\Services\City\Repositories\Contracts\CityRepository;

class EloquentCityRepository extends BaseRepository implements CityRepository
{
    protected function model()
    {
        return City::class;
    }
}
