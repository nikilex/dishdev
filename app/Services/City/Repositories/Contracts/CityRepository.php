<?php

namespace App\Services\City\Repositories\Contracts;

use App\Bases\Repository\Eloquent\Contracts\BaseRepositoryInterface;
use App\Bases\Repository\Eloquent\Contracts\RepositoryCriteriaInterface;
use App\Models\City;

interface CityRepository extends BaseRepositoryInterface, RepositoryCriteriaInterface
{
    
}
