<?php

namespace App\Services\Company;

use App\Models\Company;

class CompanyService
{
    /**
     * Получает компанию вместе с категориями
     * и блюдами по идентификатору компании
     * 
     * @param  int $id
     * @return Collection
     */
    public function getWithCompanyAndDishesById(int $id)
    {
        return Company::where('id', $id)
            ->with(['dishes.unit', 'categories'])
            ->first();
    }
}
