<?php

namespace App\Services\Dish;

use App\Models\Dish;
use App\Services\Dish\Repositories\Contracts\DishRepository;
use App\Services\Dish\Handlers\StoreDishHandler;
use App\Services\Dish\Handlers\UpdateDishHandler;
use App\Traits\BaseRepository;
use Illuminate\Support\Facades\Storage;

class DishService
{
    use BaseRepository;

    /**
     * @var DishRepository
     */
    protected $dishRepository;

    /**
     * @var StoreDishHandler
     */
    protected $storeDishHandler;

    /**
     * @var UpdateDishHandler
     */
    protected $updateDishHandler;

    public function __construct(
        DishRepository    $dishRepository,
        StoreDishHandler  $storeDishHandler,
        UpdateDishHandler $updateDishHandler
    ) {
        $this->dishRepository    = $dishRepository;
        $this->storeDishHandler  = $storeDishHandler;
        $this->updateDishHandler = $updateDishHandler;
    }

    protected function getRepository()
    {
        return $this->dishRepository;
    }

    public function getByCategory(int $categoryId, int $perPage = 15)
    {
        return Dish::where('category_id', $categoryId)->paginate($perPage);
    }

    /**
     * Получает блюдо по идентификатору
     * 
     * @param  int $dishId
     * @return Collection
     */
    public function getDishById(int $dishId)
    {
        return $this->dishRepository->find($dishId);
    }

    /**
     * Получает все блюда компании
     * @param  int $companyId
     * @return Collection
     */
    public function getCompanyDishes(int $companyId)
    {
        return $this->dishRepository
                    ->where('company_id', $companyId)
                    ->get();
    }

    /**
     * Создает новое блюдо
     * @param  array $dataDish
     * @return Collection
     */
    public function storeDish(array $dataDish)
    {
        return $this->storeDishHandler->handle($dataDish);
    }

    /**
     * Обновляет блюдо
     * @param  int   $dishId
     * @param  array $dataDish
     * @return Collection
     */
    public function updateDish(int $dishId, array $dataDish)
    {
        return $this->updateDishHandler->handle($dishId, $dataDish);
    }

    /**
     * Удаляет блюдо вместе с фото
     * @param  int $dishId
     */
    public function deleteDish(int $dishId)
    {
        $dish = $this->dishRepository->find($dishId);
        Storage::disk('public')->delete($dish->image);
        return $this->dishRepository->delete($dishId);
    }

}
