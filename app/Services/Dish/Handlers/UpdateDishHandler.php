<?php

namespace App\Services\Dish\Handlers;

use App\Services\Dish\Repositories\Contracts\DishRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UpdateDishHandler
{
    protected $dishRepository;

    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    public function handle(int $dishId, array $dataDish)
    {
        $savedImg = null;

        if (isset($dataDish['full-image'])) {
            $dish = $this->dishRepository->find($dishId);
            Storage::disk('public')->delete($dish->image);

            $path = '/dishes/user_' . Auth::id();
            $img = Image::make($dataDish['full-image']);

            // // crop image
            $img->crop(round($dataDish['width']), round($dataDish['height']), round($dataDish['left']), round($dataDish['top']));
            $img->save();

            $savedImg = Storage::disk('public')->put($path, $dataDish['full-image']);
        }

        return $this->dishRepository->update(
            $dishId,
            [
                'name'        => $dataDish['name'],
                'category_id' => $dataDish['category'],
                'unit_id'     => $dataDish['unit'],
                'description' => $dataDish['description'],
                'weight'      => $dataDish['weight'],
                'price'       => $dataDish['price'],
                'image'       => $savedImg,
                'company_id'  => Auth::user()->company_id,
            ],
            true
        );
    }
}
