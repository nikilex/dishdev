<?php

namespace App\Services\Dish\Repositories\Contracts;

use App\Bases\Repository\Eloquent\Contracts\BaseRepositoryInterface;
use App\Bases\Repository\Eloquent\Contracts\RepositoryCriteriaInterface;
use App\Models\Dish;

interface DishRepository extends BaseRepositoryInterface, RepositoryCriteriaInterface
{
    
}
