<?php

namespace App\Services\Company\Repositories\Criteria;

use App\Bases\Repository\Eloquent\Contracts\CriterionInterface;

class Search implements CriterionInterface
{

    protected string $query;

    public function __construct(string $query)
    {
        $this->query = $query;
    }

    public function apply($model)
    {
        return $model->where('name', 'like', "%{$this->query}%");
    }
}
