<?php

namespace App\Services\Dish\Repositories;

use App\Bases\Repository\Eloquent\BaseRepository;
use App\Models\Dish;
use App\Services\Dish\Repositories\Contracts\DishRepository;

class EloquentDishRepository extends BaseRepository implements DishRepository
{
    protected function model()
    {
        return Dish::class;
    }
}
