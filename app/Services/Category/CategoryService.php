<?php

namespace App\Services\Category;

use App\Models\Category;

class CategoryService
{
    public function getAll()
    {
        return Category::get();
    }

    public function paginate(array $validated)
    {
        return Category::where('company_id', $validated['company_id'])
                    ->paginate($validated['per_page']);
    }
}