<?php

namespace App\Services\Order;

use App\Models\Order;

class OrderService 
{

    /**
     * Получает заказ и его содержимое
     * по идентификатору
     * 
     * @param  int $orderId
     * @return Order
     */
    public function getOrder(int $orderId)
    {
        return Order::with('dishes')->where('id', $orderId)->first();
    }
}