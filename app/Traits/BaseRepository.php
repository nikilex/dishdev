<?php

namespace App\Traits;

use App\Exceptions\Bases\MethodNotDefined;

trait BaseRepository
{
    protected function getRepositoryVariable()
    {
        if (!method_exists($this, 'getRepository')) {
            throw new MethodNotDefined();
        }
    
        return $this->getRepository();
    }

    public function all()
    {
        return $this->getRepositoryVariable()->all();
    }

    public function first()
    {
        return $this->getRepositoryVariable()->first();
    }

    public function create(array $request)
    {
        return $this->getRepositoryVariable()->create($request);
    }

    public function update($modelId, array $updateData, bool $returnModel = false)
    {
        return $this->getRepositoryVariable()->update($modelId, $updateData, $returnModel);
    }

    public function delete(int $modelId)
    {
        return $this->getRepositoryVariable()->delete($modelId);
    }

    public function find($id)
    {
        return $this->getRepositoryVariable()->find($id);
    }

    public function whereIn(...$criterion)
    {
        return $this->getRepositoryVariable()->whereIn($criterion);
    }

    public function whereHas(...$criterion)
    {
        return $this->getRepositoryVariable()->whereHas($criterion);
    }

}
