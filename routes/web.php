<?php

use App\Http\Controllers\Admin\Categories\CategoriesController;
use App\Http\Controllers\Admin\Orders\OrdersController;
use App\Http\Controllers\Admin\Settings\SettingsController;
use App\Http\Controllers\Admin\Dishes\DishesController;
use App\Http\Controllers\Admin\IndexController as AdminIndexController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\Restaurant\RestaurantController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Тестовые роуты
 */
Route::get('city', [IndexController::class, 'city']);
/** */

Route::get('/', [IndexController::class, 'index'])->name('index');
//
Route::get('/eve', [HomeController::class, 'eve'])->name('eve');

Route::post('messages', [RestaurantController::class,'comment'])->name('messages');

Route::get('/company/{id}', [IndexController::class, 'company'])->name('company');
Route::get('/companies', [IndexController::class, 'companies'])->name('companies');
Route::get('/shop', function () {
    return view('shop');
})->name('shop');

// Корзина
Route::get('/cart', [CartController::class, 'index'])->name('cart');

// Ресторан
Route::as('rst.')
    ->prefix('/rst')
   // ->namespace('Restaurant')
    ->group(function () {
        Route::get('{id}', [RestaurantController::class,'index'])->name('index');
    });

// Админка
Route::as('admin.')
    ->prefix('/admin')
    //->namespace('Admin')
    ->middleware('auth')
    ->group(function () {

        Route::get('/', [AdminIndexController::class, 'index'])->name('index');

        // Раздел управления категориями
        
        Route::as('categories.')
            ->prefix('/categories')
           // ->namespace('Categories')
            ->group(function () {
                Route::get('/', [CategoriesController::class, 'index'])->name('index');
                Route::post('/', [CategoriesController::class, 'store'])->name('store');
                Route::put('/{id}', [CategoriesController::class, 'update'])->name('update');
                Route::delete('/{id}', [CategoriesController::class, 'destroy'])->name('destroy');
                Route::get('/create', [CategoriesController::class, 'create'])->name('create');
                Route::get('/{id}/edit', [CategoriesController::class, 'edit'])->name('edit');
            }
        );

        // Раздел управления блюдами
        Route::as('dish.')
            ->prefix('/dish')
          //  ->namespace('Dish')
            ->group(function () {
                Route::get('/', [DishesController::class, 'index'])->name('index');
                Route::get('add', [DishesController::class, 'add'])->name('add');
                Route::get('edit/{id}', [DishesController::class, 'edit'])->name('edit');
                Route::post('store', [DishesController::class, 'store'])->name('store');
                Route::post('update', [DishesController::class, 'update'])->name('update');
                Route::post('delete', [DishesController::class, 'delete'])->name('delete');

                Route::post('getDishById', [DishesController::class, 'getDishById'])->name('getDishById');
            });

        // Раздел управления заказами
        Route::as('orders.')
            ->prefix('/orders')
         //   ->namespace('Orders')
            ->group(function () {
                Route::get('/', [OrdersController::class, 'index'])->name('index');
                Route::post('/store', [OrdersController::class, 'store'])->name('store');
                Route::get('/order/{orderId}', [OrdersController::class, 'order'])->name('order');
            });

        // Раздел настройки
        Route::as('settings.')
            ->prefix('/settings')
          //  ->namespace('Settings')
            ->group(function () {
                Route::get('/', [SettingsController::class, 'index'])->name('index');
            });
    });

Route::post('/addDishInSet', [IndexController::class, 'addDishInSet'])->name('addDishInSet');

//Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
