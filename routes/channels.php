<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('company.{id}', function ($company, $id) {
    return (int) $company->id === (int) $id;
});

Broadcast::channel('private-company.{id}', function ($company, $id) {
    return (int) $company->id === (int) $id;
});

//Broadcast::routes(['middleware' => ['web', 'auth']]);
// Broadcast::channel('room.1', \App\Broadcasting\MessagesChannel::class); 

Broadcast::channel('room', function () {
    // return (int) $user->id === (int) $id;
    return true;//(int) $user->rooms->contains($room_id);
});
