<?php

use App\Http\Controllers\API\v1\Cart\CartTelegramChatController;
use App\Http\Controllers\API\v1\Categories\CategoriesController;
use App\Http\Controllers\API\v1\Dishes\DishesController ;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1'
], function () {
    Route::apiResource('dishes', DishesController::class);
    Route::get('get-dish-by-id', [DishesController::class, 'getById']);

    Route::get('dishes-by-category', [DishesController::class, 'getByCategory']);

    Route::apiResource('categories', CategoriesController::class);

    Route::apiResource('add-to-telegram-cart', CartTelegramChatController::class);
    Route::get('change-count', [CartTelegramChatController::class, 'changeCount']);
    Route::get('get-telegram-cart', [CartTelegramChatController::class, 'getTelegramCart']);

    Route::post('clear-telegram-cart', [CartTelegramChatController::class, 'clearTelegramCart']);
});
