"use strict";

/**
 * {
 * 
 *  rst_id: 15, 
 *  dishes: [
 *      { 
 *          id:    12
 *          name:  'Ролы Филадельфия'
 *          price: 124
 *          count: 1  
 *      },
 *      { 
 *          id:    17
 *          name:  'Картошка Фри'
 *          price: 250
 *          count: 3
 *      },
 *  ]
 * 
 * }
 */
setLayoutCartCount();

function getCart() {
  var cart = localStorage.getItem('cart');
  var defaultCart = {
    rst_id: null,
    dishes: []
  };

  if (cart != null) {
    return JSON.parse(cart);
  } else {
    localStorage.setItem('cart', JSON.stringify(defaultCart));
    return defaultCart;
  }
}

function setLayoutCartCount() {
  var cart = getCart();
  var spanCount = $('.count-cart');
  var allCount = 0;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = cart.dishes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var dish = _step.value;
      allCount += dish.count;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  spanCount.html(allCount);
}