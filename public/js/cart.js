

/**
 * {
 * 
 *  rst_id: 15, 
 *  dishes: [
 *      { 
 *          id:    12
 *          name:  'Ролы Филадельфия'
 *          price: 124
 *          count: 1  
 *      },
 *      { 
 *          id:    17
 *          name:  'Картошка Фри'
 *          price: 250
 *          count: 3
 *      },
 *  ]
 * 
 * }
 */

setLayoutCartCount();

function getCart() {
    let cart = localStorage.getItem('cart');
    let defaultCart = {
        rst_id: null,
        dishes: []
    }
    if (cart != null) {
        return JSON.parse(cart);
    } else {
        localStorage.setItem('cart', JSON.stringify(defaultCart));
        return defaultCart;
    }
}

function setLayoutCartCount() {
    let cart = getCart();
    let spanCount = $('.count-cart');
    let allCount = 0;

    for (let dish of cart.dishes) {
        allCount += dish.count;
    }

    spanCount.html(allCount);
}